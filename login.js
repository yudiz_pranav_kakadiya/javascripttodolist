let userEmail = document.querySelector("#txtEmail");
let userPwd = document.querySelector("#txtPwd");



function isEmail(emailVal) {
    let slash = emailVal.indexOf("/")
    if (slash >= 0) {
        return false;
    }
    let atSymbol = emailVal.indexOf("@");
    if (atSymbol < 1) return false;
    let dot = emailVal.lastIndexOf(".");
    if (dot <= atSymbol + 2) return false;
    if (dot == emailVal.length - 1) return false;
    return true;
}


function isUser(userC) {
    let slash = userC.indexOf("/")
    if (slash >= 0) {
        return false;
    }
    let plus = userC.indexOf("+")
    if (plus >= 0) {
        return false
    }
    let mul = userC.indexOf("*")
    if (mul >= 0) {
        return false
    }
    let mod = userC.indexOf("%")
    if (mod >= 0) {
        return false
    }
    let minus = userC.indexOf("-")
    if (minus >= 0) {
        return false
    }
    if (!isNaN(userC)) {
        return false
    }
    return true
}




function validateInput() {

    if (userEmail.value.trim() == "") {
        onError(userEmail, "userEmail can not empty")
    } else if (!isEmail(userEmail.value.trim())) {
        onError(userEmail, "userEmail not valid")
    }
    else {
        onSuccess(userEmail)
    }

    if (userPwd.value.trim() == "") {
        onError(userPwd, "userPwd can not empty")
    } else if (userPwd.value.trim().length < 8) {
        onError(userPwd, "paswword atleast 8")
    } else {
        onSuccess(userPwd)
    }

    return checkAllSuccess();


}



function checkAllSuccess() {
    let total = document.querySelectorAll('.form-control');

    let count = total.length - 1;
    //slice dosent work? why

    var successTotal = 0;
    for (let i = 0; i < total.length; i++) {
        if (total[i].className == 'form-control success') {
            successTotal++;
        }
    }
    console.log(successTotal)
    console.log(count)
    if (successTotal == count) {
        console.log(successTotal + " " + count)
        return true
    }
    return false;


}





function onSuccess(input) {
    let parent = input.parentElement;
    console.log(parent)
    let messageEle = parent.querySelector("small");
    messageEle.style.visibility = "hidden";
    messageEle.innerText = "";
    parent.classList.remove('error')
    parent.classList.add("success")
}

function onError(input, message) {
    let parent = input.parentElement;
    // console.log(parent)
    let messageEle = parent.querySelector("small");
    messageEle.style.visibility = "visible";
    messageEle.style.color = "red";
    messageEle.style.backgroundColor = "rgb(252, 125, 125)";
    messageEle.innerText = message;
    parent.classList.remove("sucess")
    parent.classList.add('error')

}




let flag = true;
document.querySelector(".header").addEventListener("click", (event) => {
    if (flag) {
        document.querySelector('.header').nextElementSibling.classList.remove("none")
        flag = false;
    } else {
        document.querySelector('.header').nextElementSibling.classList.add("none")
        flag = true;
    }
})


document.querySelector("#buttonLogin").addEventListener("click", (event) => {
    event.preventDefault();
    if (validateInput()) {
        checkCredential()
    }
})



function checkCredential() {

    let email = userEmail.value.trim();
    let password = userPwd.value.trim();

    let data = JSON.parse(localStorage.getItem('users'))
    console.log(data)

    if (data == null) {
        alert('kindly register first')
        signUpredirect()

    } else {
        let item = localStorage.getItem("users");
        let arrayobjfromls = JSON.parse(item);
        let found = false;
        for (var i = 0; i < arrayobjfromls.length; i++) {
            if (arrayobjfromls[i].email === email) {

                if (arrayobjfromls[i].password == password) {
                    found = true;
                    break;
                }

            } else {
                found = false;
            }
        }
        if (found) {
            alert('welcome to dashboard')
            dashboard()
        } else {
            alert("username or password invalid")

        }

    }

}



function dashboard() {
    window.location.replace("http://127.0.0.1:5500/dashboard.html");
    //u can't get back
}

function signUpredirect() {
    window.location.href = "http://127.0.0.1:5500/";
    //u can get back to sign up
}


function forgotPass() {
    window.location.href = "http://127.0.0.1:5500/forgot.html";
}