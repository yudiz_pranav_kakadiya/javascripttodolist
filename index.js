let userName = document.querySelector("#txtUserName");
let userEmail = document.querySelector("#txtEmail");
let userPwd = document.querySelector("#txtPwd");
let userConPwd = document.querySelector("#txtConPwd");
let userMobile = document.querySelector("#txtMobile");
let userGender = document.querySelector("#txtgender");
let userDob = document.querySelector("#txtDob");



function isEmail(emailVal) {
    let slash = emailVal.indexOf("/")
    if (slash >= 0) {
        return false;
    }
    let atSymbol = emailVal.indexOf("@");
    if (atSymbol < 1) return false;
    let dot = emailVal.lastIndexOf(".");
    if (dot <= atSymbol + 2) return false;
    if (dot == emailVal.length - 1) return false;
    return true;
}


function isMobile(mobile) {

    let slash = mobile.indexOf("/")
    if (slash >= 0) {
        return false;
    }

    let dot = mobile.indexOf(".")
    if (dot >= 0) {
        return false;
    }
    let plus = mobile.indexOf("+")
    if (plus >= 0) {
        return false
    }
    let minus = mobile.indexOf("-")
    if (minus >= 0) {
        return false
    }
    if (isNaN(mobile)) {
        return false
    }
    return true
}


function isYear(birth) {

    let curr = new Date().getTime();
    let dob = new Date(birth).getTime()
    let diff = 1000 * 60 * 60 * 24 * 365 * 4;
    if (curr - dob < 0) {
        return false;
    }
    if ((curr - dob) < diff) {
        return false
    }
    return true

}

function isUser(userC) {
    let slash = userC.indexOf("/")
    if (slash >= 0) {
        return false;
    }
    let plus = userC.indexOf("+")
    if (plus >= 0) {
        return false
    }
    let mul = userC.indexOf("*")
    if (mul >= 0) {
        return false
    }
    let mod = userC.indexOf("%")
    if (mod >= 0) {
        return false
    }
    let minus = userC.indexOf("-")
    if (minus >= 0) {
        return false
    }
    if (!isNaN(userC)) {
        return false
    }
    return true
}




function validateInput() {
    if (userName.value.trim() == "") {
        onError(userName, "username can not empty")
    } else if (userName.value.trim().length < 3) {
        onError(userName, "username should at least 3 charater")
    } else if (!isUser(userName.value.trim())) {
        onError(userName, "username is not accepted")
    }
    else {
        onSuccess(userName)
    }

    if (userEmail.value.trim() == "") {
        onError(userEmail, "userEmail can not empty")
    } else if (!isEmail(userEmail.value.trim())) {
        onError(userEmail, "userEmail not valid")
    }
    else {
        onSuccess(userEmail)
    }

    if (userPwd.value.trim() == "") {
        onError(userPwd, "userPwd can not empty")
    } else if (userPwd.value.trim().length < 8) {
        onError(userPwd, "paswword atleast 8")
    } else {
        onSuccess(userPwd)
    }

    if (userConPwd.value.trim() == "") {
        onError(userConPwd, "userConPwd can not empty")
    } else if (userConPwd.value.trim() !== userPwd.value.trim()) {
        onError(userConPwd, "password should be match")
    } else {
        onSuccess(userConPwd)
    }


    if (userMobile.value.trim() == "") {
        onError(userMobile, "userMobile can not empty")
    } else if (userMobile.value.trim().length != 10) {
        onError(userMobile, "userMobile should at least 10 digit")
    } else if (!isMobile(userMobile.value.trim())) {
        onError(userMobile, "userMobile should be valid")
    }
    else {
        console.log(userMobile.value)
        onSuccess(userMobile)
    }

    if (userGender.value == "") {
        onError(userGender, "userGender can not empty")
    } else {
        onSuccess(userGender)
    }

    if (userDob.value == "") {

        onError(userDob, "userDob can not empty")
    } else if (!isYear(userDob.value)) {
        onError(userDob, "user least 4 year old")
    }
    else {
        onSuccess(userDob)
    }

    return checkAllSuccess();


}



function checkAllSuccess() {
    let total = document.querySelectorAll('.form-control');

    let count = total.length - 1;
    //slice dosent work? why

    var successTotal = 0;
    for (let i = 0; i < total.length; i++) {
        if (total[i].className == 'form-control success') {
            successTotal++;
        }
    }
    console.log(successTotal)
    console.log(count)
    if (successTotal == count) {
        console.log(successTotal + " " + count)
        return true
    }
    return false;
}




function onSuccess(input) {
    let parent = input.parentElement;
    console.log(parent)
    let messageEle = parent.querySelector("small");
    messageEle.style.visibility = "hidden";
    messageEle.innerText = "";
    parent.classList.remove('error')
    parent.classList.add("success")
}

function onError(input, message) {
    let parent = input.parentElement;
    // console.log(parent)
    let messageEle = parent.querySelector("small");
    messageEle.style.visibility = "visible";
    messageEle.style.color = "red";
    messageEle.style.backgroundColor = "rgb(252, 125, 125)";
    messageEle.innerText = message;
    parent.classList.remove("sucess")
    parent.classList.add('error')

}





let flag = true;
document.querySelector(".header").addEventListener("click", (event) => {
    if (flag) {
        document.querySelector('.header').nextElementSibling.classList.remove("none")
        flag = false;
    } else {
        document.querySelector('.header').nextElementSibling.classList.add("none")
        flag = true;
    }
})


function add() {

    let allUsers = {
        userName: userName.value.trim(),
        email: userEmail.value.trim(),
        password: userPwd.value.trim(),
        mobile: userMobile.value.trim(),
        gender: userGender.value.trim(),
        dob: userDob.value.trim()
    }
    let indUser = [];
    let data = JSON.parse(localStorage.getItem('users'))
    console.log(data)

    if (data == null) {
        indUser.push(allUsers);
        localStorage.setItem('users', JSON.stringify(indUser))
        alert('Registration completed, kindly log in')
    } else {
        let item = localStorage.getItem("users");
        let arrayobjfromls = JSON.parse(item);
        let found = false;

        let email = 0;
        let user = 0;

        for (var i = 0; i < arrayobjfromls.length; i++) {
            if (allUsers.email === arrayobjfromls[i].email) {
                email = email + 1;
                if (allUsers.password === arrayobjfromls[i].password) {
                    if (allUsers.userName === arrayobjfromls[i].userName) {
                        found = true;
                        break;
                    }
                }
            }
        }
        if (found) {
            alert('you are already exist')
            dashboard()
        } else {
            if (email >= 1) {
                alert('this email already exist please use different email')
            } else {
                indUser = data
                indUser.push(allUsers);
                localStorage.setItem('users', JSON.stringify(indUser))
                alert('Registration completed, kindly log in')
            }

        }

    }

}



document.querySelector("#buttonRegister").addEventListener("click", (event) => {
    event.preventDefault();
    if (validateInput()) {
        add()
    }
})



function dashboard() {
    window.location.replace("http://127.0.0.1:5500/dashboard.html");
    //u can't get back
}

function loginredirect() {
    window.location.href = "http://127.0.0.1:5500/login.html";
    //u can get back to sign up
} 